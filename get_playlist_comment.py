import urllib.parse
import urllib.request
import urllib.error
import http.cookiejar
import ssl

import os
import json
import gzip
import uuid

import time, timeit
import random
from datetime import datetime
from datetime import timedelta

import snowflake.client

from concurrent import futures


import db_handler
import utils


playlist_comment_url = "http://localhost:3000/comment/playlist"

local_avatar_path = "D:/yunmusic_dev_v3/avatar"


def get_comments_by_playlist_id(playlist_id):
    comments_url = playlist_comment_url + "?id=" + str(playlist_id)

    resp_json = utils.get_json(comments_url)

    comments = resp_json['comments']

    for comment in comments:
        comment_id = comment['commentId']
        content = comment['content']
        create_time = utils.get_datetime_from_seconds(comment['time'])
        user = comment['user']
        user_id = user['userId']
        result = utils.get_and_save_user(user_id, local_avatar_path)

        be_replied = comment['beReplied']

        if len(be_replied) > 0:
            replied_comment = be_replied[0]
            replied_user_id = replied_comment['user']['userId']
            utils.get_and_save_user(replied_user_id, local_avatar_path)
            replied_content = comment['content']
            reply_comment_id = snowflake.client.get_guid()
            if not db_handler.exist_comment(replied_user_id, replied_comment):
                db_handler.add(db_handler.Comment(id=snowflake.client.get_guid(),
                                                  user_id=replied_user_id,
                                                  target_id=playlist_id,
                                                  type=1,
                                                  content=replied_content,
                                                  create_time=create_time))

            if not db_handler.exist(comment_id, db_handler.Comment):
                db_handler.add(db_handler.Comment(id=comment_id,
                                                  user_id=user_id,
                                                  target_id=playlist_id,
                                                  type=1,
                                                  reply_comment_id=reply_comment_id,
                                                  content=content,
                                                  create_time=create_time))
        else:
            if not db_handler.exist(comment_id, db_handler.Comment):
                db_handler.add(db_handler.Comment(id=comment_id,
                                                  user_id=user_id,
                                                  target_id=playlist_id,
                                                  type=1,
                                                  content=content,
                                                  create_time=create_time))


if __name__ == '__main__':
    ids = utils.get_ids_from_db(db_handler.Playlist)
    utils.get_data_from_ids(get_comments_by_playlist_id, ids)

