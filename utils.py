import urllib.parse
import urllib.request
import urllib.error
import http.cookiejar
import ssl

import os
import json
import gzip
import uuid

import time, timeit
import random
from datetime import datetime
from datetime import timedelta

import snowflake.client

from concurrent import futures


import db_handler


headers = [
    ("Accept", "*/*"),
    ("Accept-Encoding", "gzip, deflate, br"),
    ("Accept-Language", "zh-CN,zh;q=0.9"),
    ("User-Agent",
     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"),
]
opener = urllib.request.build_opener()
opener.addheaders = headers


user_detail_url = "http://localhost:3000/user/detail"



def get_datetime_from_seconds(seconds):
    time_array = time.localtime(float(seconds)/1000)  # 1970秒数
    # print(time_array)
    otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", time_array)
    datetime1 = datetime.strptime(otherStyleTime, "%Y-%m-%d %H:%M:%S")
    return datetime1


def download_avatar(local_avatar_path, avatar_url, avatar_name):

    if not os.path.exists(local_avatar_path):
        os.makedirs(local_avatar_path)

    try:
        urllib.request.urlretrieve(avatar_url, os.path.join(local_avatar_path, avatar_name))
    except Exception as e:
        raise e
    print("download", avatar_name, "ok")


def download_file(local_path, remote_url, filename):

    if not os.path.exists(local_path):
        os.makedirs(local_path)

    try:
        urllib.request.urlretrieve(remote_url, os.path.join(local_path, filename))
    except Exception as e:
        raise e
    print("download", filename, "ok")


def get_json(path):
    print("get from", path)
    try:
        resp = opener.open(path)
    except Exception as e:
        print(e)
        print("ignore error")
        return False

    if resp.info().get('Content-Encoding') == 'gzip':
        resp_decode = gzip.decompress(resp.read()).decode("utf-8")
    else:
        resp_decode = resp.read().decode("utf-8")
    resp_json = json.loads(resp_decode)
    return resp_json


def get_and_save_user(user_id, local_avatar_path):

    path = user_detail_url + "?uid=" + str(user_id)

    resp_json = get_json(path)

    profile = resp_json['profile']

    id = profile['userId']
    nickname = profile['nickname']
    description = profile['description']
    avatar_img_id = profile['avatarImgId']
    avatar_img_url = profile['avatarUrl']

    if db_handler.exist(id, db_handler.User):
        return False

    # 下载用户头像
    download_avatar(local_avatar_path, avatar_img_url, str(avatar_img_id) + ".jpg")

    # 保存用户信息到数据库
    db_handler.add(db_handler.User(id=id,
                                   nickname=nickname,
                                   description=description,
                                   avatar_img_id=avatar_img_id,
                                   password="111111",
                                   user_type=0))
    print("add", nickname, "ok")
    return True


def get_ids_from_db(table):
    ts = db_handler.query_all(table)
    ids = [t.id for t in ts]
    return ids


def get_data_from_ids(method, ids):
    workers = min(len(ids), 20)
    with futures.ThreadPoolExecutor(workers) as executor:
        executor.map(method, ids)