import urllib.parse
import urllib.request
import urllib.error
import http.cookiejar
import ssl

import os
import json
import gzip
import uuid

import time, timeit
import random
from datetime import datetime
from datetime import timedelta

from concurrent import futures


import db_handler


headers = [
    ("Accept", "*/*"),
    ("Accept-Encoding", "gzip, deflate, br"),
    ("Accept-Language", "zh-CN,zh;q=0.9"),
    ("User-Agent",
     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"),
]
opener = urllib.request.build_opener()
opener.addheaders = headers

local_cover_path = "D:/yunmusic_dev/cover/playlist"

local_avatar_path = "D:/yunmusic_dev/avatar"

root_url = "http://localhost:3000/top/playlist"


def get_pagelist_with_offset(offset):
    pagelist_url = root_url + "?limit=20&order=hot&offset=" + str(offset)
    print("get pagelists from", pagelist_url)
    try:
        resp = opener.open(pagelist_url)
    except Exception as e:
        print(e)
        print("ignore error")
        return False

    if resp.info().get('Content-Encoding') == 'gzip':
        resp_decode = gzip.decompress(resp.read()).decode("utf-8")
    else:
        resp_decode = resp.read().decode("utf-8")
    resp_json = json.loads(resp_decode)

    print(resp_json)

    playlists = resp_json['playlists']

    for playlist in playlists:
        print(playlist)
        id = playlist['id']
        name = playlist['name']
        description = playlist['description']
        user_id = playlist['userId']
        play_count = playlist['playCount']
        cover_img_id = playlist['coverImgId']
        cover_img_url = playlist['coverImgUrl']
        create_time = get_datetime_from_seconds(playlist['createTime'])
        update_time = get_datetime_from_seconds(playlist['updateTime'])

        if db_handler.exist_playlist(id):
            continue

        # 下载歌单封面
        download_cover(cover_img_url, str(cover_img_id)+".jpg")
        # 保存歌单到数据库
        playlist_db = db_handler.Playlist(id=id,
                                          user_id=user_id,
                                          name=name,
                                          description=description,
                                          play_count=play_count,
                                          cover_img_id=cover_img_id,
                                          update_time=update_time,
                                          create_time=create_time)
        db_handler.add_playlist(playlist_db)
        print("add", name, "ok")

        # 获取创建歌单的用户
        creator = playlist['creator']
        id = creator['userId']
        nickname = creator['nickname']
        description = creator['description']
        avatar_img_id = creator['avatarImgId']
        avatar_img_url = creator['avatarUrl']

        if db_handler.exist(id, db_handler.User):
            continue

        # 下载用户头像
        download_avatar(avatar_img_url, str(avatar_img_id) + ".jpg")

        # 保存用户信息到数据库
        db_handler.add(db_handler.User(id=id,
                                       nickname=nickname,
                                       description=description,
                                       avatar_img_id=avatar_img_id,
                                       password="111111",
                                       user_type=0))
        print("add", nickname, "ok")

    # 随机暂停一段时间
    seconds = random.randint(1, 5)
    time.sleep(seconds)


def download_avatar(avatar_url, avatar_name):

    if not os.path.exists(local_avatar_path):
        os.makedirs(local_avatar_path)

    try:
        urllib.request.urlretrieve(avatar_url, os.path.join(local_avatar_path, avatar_name))
    except Exception as e:
        raise e
    print("download", avatar_name, "ok")


def download_cover(cover_url, cover_name):

    if not os.path.exists(local_cover_path):
        os.makedirs(local_cover_path)

    try:
        urllib.request.urlretrieve(cover_url, os.path.join(local_cover_path, cover_name))
    except Exception as e:
        raise e
    print("download", cover_name, "ok")


def get_datetime_from_seconds(seconds):
    time_array = time.localtime(float(seconds)/1000)  # 1970秒数
    # print(time_array)
    otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", time_array)
    datetime1 = datetime.strptime(otherStyleTime, "%Y-%m-%d %H:%M:%S")
    return datetime1


def get_pagelist(offset, total):

    workers = min(total-offset, 20)
    with futures.ThreadPoolExecutor(workers) as executor:
        executor.map(get_pagelist_with_offset, list(range(offset, total)))

    # for i in range(offset, total):
    #     get_pagelist_with_offset(root_url, 0)
    #     seconds = random.randint(1, 5)
    #     time.sleep(seconds)


if __name__ == "__main__":
    get_pagelist(0, 100)
