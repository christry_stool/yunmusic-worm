import urllib.parse
import urllib.request
import urllib.error
import http.cookiejar
import ssl

import os
import json
import gzip
import uuid

import time, timeit
import random
from datetime import datetime
from datetime import timedelta

import snowflake.client

from concurrent import futures


import db_handler
import utils


song_url = "http://localhost:3000/song/detail"


def get_song_duration(song_id):

    url = song_url + "?ids=" + str(song_id)
    resp_json = utils.get_json(url)

    song = resp_json['songs'][0]
    duration = song['dt']

    db_handler.set_song_duration(song_id, duration)

    print("set", song_id, "ok")


if __name__ == '__main__':
    # get_song_duration(425828194)
    ids = utils.get_ids_from_db(db_handler.Song)
    utils.get_data_from_ids(get_song_duration, ids)