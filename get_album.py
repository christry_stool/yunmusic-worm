import urllib.parse
import urllib.request
import urllib.error
import http.cookiejar
import ssl

import os
import json
import gzip
import uuid

import time, timeit
import random
from datetime import datetime
from datetime import timedelta

import snowflake.client

from concurrent import futures


import db_handler
import utils


song_detail_url = "http://localhost:3000/song/detail"
album_url = "http://localhost:3000/album"
artist_url = "http://localhost:3000/artists"

local_artist_cover_path = "D:/yunmusic_dev_v3/cover/artist"
local_album_cover_path = "D:/yunmusic_dev_v3/cover/album"


def get_album_from_song_id(song_id):
    url = song_detail_url + "?ids=" + str(song_id)
    resp_json = utils.get_json(url)

    song = resp_json['songs'][0]
    al = song['al']
    album_id = al['id']
    url = album_url + "?id=" + str(album_id)
    resp_json = utils.get_json(url)

    album = resp_json['album']

    # 获取歌手
    ars = album["artists"]
    for ar in ars:
        artist_id = ar['id']
        url = artist_url + "?id=" + str(artist_id)
        resp_json = utils.get_json(url)

        artist = resp_json['artist']
        artist_name = artist['name']
        pic_url_id = artist['picId']
        pic_url = artist['picUrl']
        description = artist['briefDesc']

        if not db_handler.exist(artist_id, db_handler.Artist):
            utils.download_file(local_artist_cover_path, pic_url, str(pic_url_id) + ".jpg")
            db_handler.add(db_handler.Artist(id=artist_id,
                                             name=artist_name,
                                             description=description,
                                             pic_url_id=pic_url_id))

        if not db_handler.exist_artist_has_album(artist_id, album_id):
            db_handler.add(db_handler.ArtistHasAlbum(id=snowflake.client.get_guid(),
                                                     album_id=album_id,
                                                     artist_id=artist_id))

    album_name = album['name']
    pic_url = album['picUrl']
    pic_url_id = album['picId']
    description = album['description']
    publish_time = utils.get_datetime_from_seconds(album['publishTime'])
    if not db_handler.exist(album_id, db_handler.Album):
        utils.download_file(local_album_cover_path, pic_url, str(pic_url_id) + ".jpg")
        db_handler.add(db_handler.Album(id=album_id,
                                        name=album_name,
                                        pic_url_id=pic_url_id,
                                        description=description,
                                        publish_time=publish_time))

    if not db_handler.exist_album_has_song(album_id, song_id):
        db_handler.add(db_handler.AlbumHasSong(id=snowflake.client.get_guid(),
                                               album_id=album_id,
                                               song_id=song_id))

    print("add", album_name, "ok")
    # 随机暂停一段时间
    seconds = random.randint(1, 10)
    time.sleep(seconds)


if __name__ == '__main__':
    ids = utils.get_ids_from_db(db_handler.Song)
    utils.get_data_from_ids(get_album_from_song_id, ids)
    # get_album_from_song_id("101392")




